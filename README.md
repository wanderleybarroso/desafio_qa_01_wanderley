## Setup Capybara/Ruby

É recomendado instalar o rbenv com Homebrew
```
brew install rbenv      # instala o rbenv
```
Em seguida adicione em seu arquivo ~/.bash_profile a linha abaixo:
```
eval "$(rbenv init -)"
```

### > Instalando versões do Ruby

Instalar uma versão do ruby
```
rbenv install x.x.x
```

### > Instalando Chromedriver

Instalar o bundler:
```
npm install chromedriver ou brew cask install chromedriver
```

### > Instalando selenium-webdriver

Instalar selenium-webdriver: 
```
npm i selenium-webdriver
```

### > Instalando gems

Instalar gem capybara:
```
gem install capybara -v 2.7.1
```
Instalar gem cucumber: 
```
gem install cucumber -v 2.4.0
```

### > Instalando bundler

Instalar o bundler:
```
install bundler
```
Instalar as dependências(gems):
```
bundle install
```

### > Executando o teste funcional 

Na raiz do projeto execultar:
```
bundle exec cucumber -p default features
```

## Setup HTTParty

instalar o servidor Json-server:
```
npm install -g json-server
```

### > Executando o teste api

Acessar a pasta \webservice, e execultar:
```
rspec spec/list_film.rb --format documentation
```