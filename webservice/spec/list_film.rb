require 'httparty'
require 'spec_helper'
require 'json'

class Api
  include HTTParty
  base_uri 'http://swapi.co/api/'
end

RSpec.describe 'lista de filmes - requisição GET' do
  it 'efetuando requisição GET' do
    @response = Api.get('/films/')
    parse = JSON.parse(@response.body)
    size = parse.size
    i = 0 
    while i <= size do
      results = parse['results'][i]['director'] == "George Lucas" && parse['results'][i]['producer'] == "Rick McCallum"
        if results == true
          puts parse['results'][i]['title']
        end
      i += 1
    end
    expect(@response.code).to eql(200)
  end
end
