Dado("que esteja na página de login do sistema") do
  @home = Login.new 
  @home.load
  @home.home_page
end
  
Dado("realizo login") do
  @home.realizar_login
end

Dado("que esteja no Dashboard") do
  @cadRoom = CadastrarRoom.new
  @cadRoom.visualizar_pagina_dashboard
end

Dado("toco em ADD ROOM") do
  @cadRoom.clicar_add_room
end

Quando("preencher e submeter as informações das abas General e Amenities") do
  @cadRoom.preencher_dados_general
end

Então("um novo room deve ser cadastrado") do
  @cadRoom.visualiar_cadastro
end