# language: pt
Funcionalidade: Cadastrar_Room
  Eu como usuário desejo cadastrar quartos para clientes, podendo adicionar itens a mais para o usuário 
  ao quarto que será cadastrado

Contexto:
Dado que esteja na página de login do sistema
E realizo login

Cenário: Cadastrar um novo Room
  Dado que esteja no Dashboard
  E toco em ADD ROOM
  Quando preencher e submeter as informações das abas General e Amenities 
  Então um novo room deve ser cadastrado