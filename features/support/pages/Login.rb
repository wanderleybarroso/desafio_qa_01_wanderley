class Login < SitePrism::Page
  set_url "https://www.phptravels.net/supplier"

  def initialize
    @email = 'Email'
    @password = 'Password'
    @home =  "//strong[contains(text(),'Login Panel')]"
    @btm_button = 'Login'
  end

  def home_page
    page.has_xpath?(@home)
  end  

  def realizar_login  
    login = CREDENTIALS[:login]      
    fill_in @email, :with => (login[:email])
    fill_in @password, :with => (login[:password])
    click_button @btm_button
  end
end
