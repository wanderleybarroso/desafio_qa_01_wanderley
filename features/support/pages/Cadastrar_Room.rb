class CadastrarRoom < SitePrism::Page
  def initialize
    @page_Dashboard = "//body[contains(@class,'pace-done')]/div[contains(@class,'wrapper')]/div/div[2]"
    @hotels = 'Hotels'
    @add_room = 'Add Room'
    @roomstatusEnabled = "//select[@name='roomstatus']/option[@value='Yes']"
    @openListrooms = "//span[contains(text(),'Room Type')]"
    @select_type_room = "//div[contains(text(),'Studio Apartment With Creek View')]"
    @price = "//input[@placeholder='Price']"
    @quantity = "//input[@placeholder='Quantity']"
    @minimum_stay = "//input[@placeholder='Minimum Stay']"
    @max_adults = "//input[@placeholder='Max Adults']"
    @max_children = "//input[@placeholder='Max Children']"
    @extra_beds = "//input[@placeholder='Extra beds']"
    @beds_charges = "//input[@placeholder='Beds charges']"
    @click_menu_amenities = 'Amenities'
    @select_bathroom_phone = "//label[contains(text(),'Bathroom phone')]"
    @select_air_conditioning = "//label[contains(text(),'Air conditioning')]"
    @select_lcd_tv = "//label[contains(text(),'LCD TV')]"
    @select_minibar = "//label[contains(text(),'Minibar')]"
    @btn_submit = '#add'
    @message_register = "//h4[@class='ui-pnotify-title']"
  end

  def visualizar_pagina_dashboard
    page.has_xpath?(@page_Dashboard)
  end

  def clicar_add_room
    click_link(@hotels)
    click_link(@add_room)
  end

  def preencher_dados_general
    itenscad = CREDENTIALS[:Dados_cadastrais]      
    find(:xpath, @roomstatusEnabled).click
    find(:xpath, @openListrooms).click
    find(:xpath, @select_type_room ).click
    find(:xpath, @price).set(itenscad[:price])
    find(:xpath, @quantity).set(itenscad[:quantity])
    find(:xpath, @minimum_stay).set(itenscad[:minimum_stay])
    find(:xpath, @max_adults).set(itenscad[:max_adults])
    find(:xpath, @max_children).set(itenscad[:max_children])
    find(:xpath, @extra_beds).set(itenscad[:extra_beds])
    find(:xpath, @beds_charges).set(itenscad[:beds_charges])
    incluir_amenities
    find(:css, @btn_submit).click
  end

  def incluir_amenities
    click_link(@click_menu_amenities)
    find(:xpath, @select_bathroom_phone).click
    find(:xpath, @select_air_conditioning).click
    find(:xpath, @select_lcd_tv).click
    find(:xpath, @select_minibar).click     
  end

  def visualiar_cadastro
    raise ArgumentError, "Mensagem não exibida", caller if page.has_content?(@message_register) 
    raise ArgumentError, "Cadastro não efetuado", caller if page.has_xpath?(@select_type_room)
  end  
     
end